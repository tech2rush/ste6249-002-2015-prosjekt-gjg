#ifndef PROJECTILE_H
#define PROJECTILE_H
#define gravity -0.0981


#include <parametrics/gmpsphere>


class Projectile: public GMlib::PSphere<float> {
public:
    using PSphere::PSphere;

  Projectile(){}

  Projectile(GMlib::Vector<float,3> direction){
      _direction = direction;
      _radius = 0.50f;
      this->_dm = GMlib::GM_DERIVATION_EXPLICIT;
  }

  ~Projectile() {

    //if(m_test01)
    //  remove(test_01_torus.get());
  }

  void test01() {
   m_test01 = true;
  }


protected:
  void localSimulate(double dt) override {
    gravity_acceleration += dt;
    this->translate(_direction * 10.0f * dt);
    this->translate(GMlib::Vector<float,3>(0.0f,0.0f,1.0f)*
                    gravity*gravity_acceleration*gravity_acceleration);
  }

private:
  bool m_test01 {false};
  GMlib::Vector<float,3> _direction;
  double gravity_acceleration = 0.0f;
}; // END class TestTorus



#endif // PROJECTILE_H
