//#include "../gmpsurf.h"
#include "parametrics/gmpsurf.h"
#include "planevisualizer.h"

// gmlib
#include <opengl/gmopengl.h>
#include <opengl/gmopenglmanager.h>
#include <scene/gmscene.h>
#include <scene/camera/gmcamera.h>
#include <scene/light/gmlight.h>
#include <scene/utils/gmmaterial.h>

// stl
#include <set>
#include <string>
#include <iostream>
#include <fstream>
#include <QDebug>

//namespace GMlib {
//  template <typename T, int n>
    PlaneVisualizer::PlaneVisualizer()
    : _no_strips(0), _no_strip_indices(0), _strip_size(0) {

    initShaderProgram();

    _color_prog.acquire("color");
    assert(_color_prog.isValid());

    _vbo.create();
    _ibo.create();
    _nmap.create(GL_TEXTURE_2D);
  }

  //template <typename T, int n>
  PlaneVisualizer::PlaneVisualizer(const PlaneVisualizer& copy)
    : PSurfVisualizer<float,3>(copy), _no_strips(0), _no_strip_indices(0), _strip_size(0) {

    initShaderProgram();

    _color_prog.acquire("color");
    assert(_color_prog.isValid());


    _vbo.create();
    _ibo.create();
    _nmap.create(GL_TEXTURE_2D);
  }

//  template <typename T, int n>
  inline
  void PlaneVisualizer::render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const {

    const GMlib::Camera* cam = renderer->getCamera();
    const GMlib::HqMatrix<float,3> &mvmat = obj->getModelViewMatrix(cam);
    const GMlib::HqMatrix<float,3> &pmat = obj->getProjectionMatrix(cam);
//    const SqMatrix<float,3> &nmat = obj->getNormalMatrix(cam);

    GMlib::SqMatrix<float,3>        nmat = mvmat.getRotationMatrix();
    nmat.invertOrthoNormal();
    nmat.transpose();

    this->glSetDisplayMode();

    _prog.bind(); {

      // Model view and projection matrices
      _prog.setUniform( "u_mvmat", mvmat );
      _prog.setUniform( "u_mvpmat", pmat * mvmat );
      _prog.setUniform( "u_nmat", nmat );

      // Lights
      _prog.setUniformBlockBinding( "Lights", renderer->getLightUBO(), 0 );

      // Material
      const GMlib::Material &m = obj->getMaterial();
      _prog.setUniform( "u_mat_amb", m.getAmb() );
      _prog.setUniform( "u_mat_dif", m.getDif() );
      _prog.setUniform( "u_mat_spc", m.getSpc() );
      _prog.setUniform( "u_mat_shi", m.getShininess() );

      // Normal map
      _prog.setUniform( "u_nmap", _nmap, (GLenum)GL_TEXTURE0, 0 );

      // Get vertex and texture attrib locations
      GMlib::GL::AttributeLocation vert_loc = _prog.getAttributeLocation( "in_vertex" );
      GMlib::GL::AttributeLocation tex_loc = _prog.getAttributeLocation( "in_tex" );
      //
      GMlib::GL::AttributeLocation normal_loc = _prog.getAttributeLocation("in_normal");

      // Bind and draw
      _vbo.bind();
      _vbo.enable( vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );
      _vbo.enable( tex_loc,  2, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(3*sizeof(GLfloat)) );
      //
      _vbo.enable( normal_loc, 3, GL_FLOAT, GL_TRUE, sizeof(GMlib::GL::GLVertexNormal), reinterpret_cast<const GLvoid*>(sizeof(GMlib::GL::GLVertex) ) );

      draw();

      _vbo.disable( vert_loc );
      _vbo.disable( tex_loc );
      //
      _vbo.disable( normal_loc);

      _vbo.unbind();

    } _prog.unbind();
  }

  //template <typename T, int n>
  //inline
  void PlaneVisualizer::draw() const {

    _ibo.bind();
    for( unsigned int i = 0; i < _no_strips; ++i )
      _ibo.drawElements( GL_TRIANGLE_STRIP, _no_strip_indices, GL_UNSIGNED_INT, reinterpret_cast<const GLvoid *>(i * _strip_size) );
    _ibo.unbind();
  }

  //template <typename T, int n>
  //inline
  void PlaneVisualizer::replot(
    const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,

    const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
    int /*m1*/, int /*m2*/, int /*d1*/, int /*d2*/,
    bool closed_u, bool closed_v
  ) {

    PSurfVisualizer<float, 3>::fillStandardVBO( _vbo, p );
    PSurfVisualizer<float, 3>::fillTriangleStripIBO( _ibo, p.getDim1(), p.getDim2(), _no_strips, _no_strip_indices, _strip_size );
    PSurfVisualizer<float, 3>::fillNMap( _nmap, normals, closed_u, closed_v );
  }

  //template <typename T, int n>
  //inline
  void PlaneVisualizer::renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const {

    _color_prog.bind(); {
      _color_prog.setUniform( "u_color", color );
      _color_prog.setUniform( "u_mvpmat", obj->getModelViewProjectionMatrix(renderer->getCamera()) );
      GMlib::GL::AttributeLocation vertice_loc = _color_prog.getAttributeLocation( "in_vertex" );

      _vbo.bind();
      _vbo.enable( vertice_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GMlib::GL::GLVertexTex2D), reinterpret_cast<const GLvoid *>(0x0) );

      draw();

      _vbo.disable( vertice_loc );
      _vbo.unbind();

    } _color_prog.unbind();
  }

  //template<typename T,int n>
  void PlaneVisualizer::initShaderProgram() {

    const std::string prog_name    = "psurf_default_prog";
    if( _prog.acquire(prog_name) ) return;


    std::string vs_src =
        GMlib::GL::OpenGLManager::glslDefHeader150Source();

    std::ifstream file("noisemapper.vs");

    if (!file) qDebug() << "Error opening noisemapper.vs";
    std::string temp_string;
    while(std::getline(file, temp_string)){
        vs_src += temp_string;
        vs_src += "\n";
        temp_string = "";
    }
    file.close();

    std::string fs_src =
        GMlib::GL::OpenGLManager::glslDefHeader150Source() +
        GMlib::GL::OpenGLManager::glslFnComputeLightingSource() +

        "uniform sampler2D u_nmap;\n"
        "uniform mat4      u_mvmat;\n"
        "uniform mat3      u_nmat;\n"
        "\n"
        "uniform vec4      u_mat_amb;\n"
        "uniform vec4      u_mat_dif;\n"
        "uniform vec4      u_mat_spc;\n"
        "uniform float     u_mat_shi;\n"
        "\n"
        "smooth in vec3    ex_pos;\n"
        "smooth in vec2    ex_tex;\n"
        "\n"
        "out vec4 gl_FragColor;\n"
        "\n"
        "void main() {\n"
        "\n"
        "  vec3 nmap_normal = texture( u_nmap, ex_tex.ts).xyz;\n"
        "  vec3 normal = normalize( u_nmat * nmap_normal );\n"
        "\n"
        "  Material mat;\n"
        "  mat.ambient   = u_mat_amb;\n"
        "  mat.diffuse   = u_mat_dif;\n"
        "  mat.specular  = u_mat_spc;\n"
        "  mat.shininess = u_mat_shi;\n"
        "\n"
        "  gl_FragColor = computeLighting( mat, ex_pos, normal );\n"
        "\n"
        "}\n"
        ;

    bool compile_ok, link_ok;

    GMlib::GL::VertexShader vshader;
    vshader.create("plane_vs");
    vshader.setPersistent(true);
    vshader.setSource(vs_src);
    compile_ok = vshader.compile();
    assert(compile_ok);

    GMlib::GL::FragmentShader fshader;
    fshader.create("plane_fs");
    fshader.setPersistent(true);
    fshader.setSource(fs_src);
    compile_ok = fshader.compile();
    if( !compile_ok ) {
      std::cout << "Src:" << std::endl << fshader.getSource() << std::endl << std::endl;
      std::cout << "Error: " << fshader.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    _prog.create(prog_name);
    _prog.setPersistent(true);
    _prog.attachShader(vshader);
    _prog.attachShader(fshader);
    link_ok = _prog.link();
    assert(link_ok);
  }

//} // END namespace GMlib
