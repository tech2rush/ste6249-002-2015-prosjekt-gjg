#ifndef PLANEVISUALIZER_H
#define PLANEVISUALIZER_H

#include "parametrics/gmpsurfvisualizer"

// gmlib
#include <opengl/bufferobjects/gmvertexbufferobject.h>
#include <opengl/bufferobjects/gmindexbufferobject.h>
#include <opengl/bufferobjects/gmuniformbufferobject.h>
#include <opengl/gmtexture.h>
#include <opengl/gmprogram.h>
#include <opengl/shaders/gmvertexshader.h>
#include <opengl/shaders/gmfragmentshader.h>
#include <scene/render/gmdefaultrenderer.h>


//namespace GMlib {

  //template <typename T, int n>
  //class PlaneVisualizer : public PSurfVisualizer<T,n> {
  class PlaneVisualizer : public GMlib::PSurfVisualizer<float, 3> {
    GM_VISUALIZER(PlaneVisualizer)
  public:
    PlaneVisualizer();
    PlaneVisualizer( const PlaneVisualizer& copy );

    void          render( const GMlib::SceneObject* obj, const GMlib::DefaultRenderer* renderer ) const;
    void          renderGeometry( const GMlib::SceneObject* obj, const GMlib::Renderer* renderer, const GMlib::Color& color ) const;

    virtual void  replot( const GMlib::DMatrix< GMlib::DMatrix< GMlib::Vector<float, 3> > >& p,
                          const GMlib::DMatrix< GMlib::Vector<float, 3> >& normals,
                          int m1, int m2, int d1, int d2,
                          bool closed_u, bool closed_v
    );

  private:
    GMlib::GL::Program                 _prog;
    GMlib::GL::Program                 _color_prog;

    GMlib::GL::VertexBufferObject      _vbo;
    GMlib::GL::IndexBufferObject       _ibo;
    GMlib::GL::Texture                 _nmap;

    GLuint                      _no_strips;
    GLuint                      _no_strip_indices;
    GLsizei                     _strip_size;

    void                        draw() const;

    void                        initShaderProgram();

  }; // END class PlaneVisualizer

//} // END namespace GMlib

// Include PlaneVisualizer class function implementations
//#include "planevisualizer.cpp"


#endif // PLANEVISUALIZER_H
