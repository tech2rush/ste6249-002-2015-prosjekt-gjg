#ifndef CLOSED_CYLINDER_H
#define CLOSED_CYLINDER_H


#include <parametrics/gmpcylinder>
#include <parametrics/gmpcircularsurface>
#include <gmParametricsModule>


class ClosedCylinder:public GMlib::PCylinder<float>{
public:
    using PCylinder::PCylinder;

  ClosedCylinder(){
  }

  //ClosedCylinder(float rx, float ry, float h)
      //PCylinder::PCylinder(rx, ry, h);


  ~ClosedCylinder() {

    if(gp_test){
        remove(this->cylinder_top.get());
        remove(this->cylinder_bottom.get());
    }
  }

  void close_cylinder(int m1, int m2, int d1, int d2) {
    GMlib::Vector<float,3> d = evaluate(0.0f, 0.0f, 0, 0)[0][0];
    //auto surface_visualizer = this->getVisualizers()[0]->makeCopy();
    //auto surface_visualizer = new GMlib::PSurfVisualizer<float,3> (this->getVisualizers());
    auto surface_visualizer = new GMlib::PSurfNormalsVisualizer<float,3>;
    //GMlib::PSurfVisualizer<float,3> surface_visualizer = GMlib::PSurfVisualizer <float,3>(this->getVisualizers()[0]);
    //How to copy the parents visualizer without hijacking it?

    //Create top of closed cylinder
    cylinder_top= std::make_shared<GMlib::PCircularSurface<float> >(this->getRadiusX());
    cylinder_top->translate(GMlib::Vector<float,3>(0,0,this->getHeight()/2));
    cylinder_top->translate(d+d.getNormalized()* -1 * this->getRadiusX());
    //cylinder_top->insertVisualizer(surface_visualizer);
    cylinder_top->toggleDefaultVisualizer();
    cylinder_top->replot(m1,m2,d1,d2);
    insert(cylinder_top.get());

    //Create bottom of closed cylinder
    cylinder_bottom= std::make_shared<GMlib::PCircularSurface<float> >(this->getRadiusX());
    cylinder_bottom->rotate(GMlib::Angle(180), GMlib::Vector<float,3>(0.0f, 1.0f, 0.0f));
    cylinder_bottom->translate(GMlib::Vector<float,3>(0,0, this->getHeight()/2));
    cylinder_bottom->translate(d+d.getNormalized()* -1 * this->getRadiusX());
    //cylinder_bottom->insertVisualizer(surface_visualizer);
    cylinder_bottom->toggleDefaultVisualizer();
    //cylinder_bottom->replot(m1,m2,d1,d2);
    insert(cylinder_bottom.get());

    gp_test = true;
  }

protected:
  void localSimulate(double dt) override {
  }

private:
  bool gp_test {false};
  std::shared_ptr<GMlib::PCircularSurface<float> > cylinder_top{nullptr};
  std::shared_ptr<GMlib::PCircularSurface<float> > cylinder_bottom{nullptr};
}; // END class ClosedCylinder



#endif // CLOSED_CYLINDER_H
