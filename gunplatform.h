#ifndef GUNPLATFORM_H
#define GUNPLATFORM_H

#include <QObject>
#include <QDebug>

#include <memory>
#include <scene/gmsceneobject.h>
#include <scene/sceneobjects/gmsphere3d.h>

#include "closed_cylinder.h"


class GunPlatform:public QObject, public ClosedCylinder{
  Q_OBJECT
public:
    using PCylinder::PCylinder;

  GunPlatform(){
  }

  ~GunPlatform() {

    if(gp_test){
        remove(verticalPivot.get());
        remove(barrelCylinder.get());
    }
  }

  void construct_arm() {

      GMlib::Vector<float,3> d = evaluate(0.0f, 0.0f, 0, 0)[0][0];

      verticalPivot = std::make_shared<ClosedCylinder>(1.0f, 1.0f, 2.0f);
      verticalPivot->translate(GMlib::Vector<float,3>(0,0,this->getHeight()));
      verticalPivot->toggleDefaultVisualizer();
      verticalPivot->replot(2,20,1,1);
      insert(verticalPivot.get());
      verticalPivot->close_cylinder(2,20,1,1);

      /*
      firstJoint = std::make_shared<ClosedCylinder>(0.5f, 0.5f, 6.0f);
      firstJoint->translate(
                  GMlib::Vector<float,3>(
                    verticalPivot->getRadiusX() + firstJoint->getRadiusX(),
                    0,
                    this->getHeight()/2 + firstJoint->getHeight()/2 + verticalPivot->getHeight()
                    -firstJoint->getRadiusX())
                            );
      firstJoint->toggleDefaultVisualizer();
      firstJoint->replot(2,20,1,1);
      insert(firstJoint.get());
      firstJoint->close_cylinder(2,20,1,1);
      */

      barrelCylinder = std::make_shared<ClosedCylinder>(0.5f, 0.5f, 5.0f);
      barrelCylinder->rotate(GMlib::Angle(90), GMlib::Vector<float,3>(1.0f, 0.0f, 0.0f));
      barrelCylinder->translate(
                  GMlib::Vector<float,3>(
                    0,
                    0,
                    2.0f
                    ));
      barrelCylinder->toggleDefaultVisualizer();
      barrelCylinder->replot(2,20,1,1);
      verticalPivot->insert(barrelCylinder.get());
      barrelCylinder->close_cylinder(2,20,1,1);

      gp_test = true;
  }

GMlib::Vector<float,3> getGunVector(){
    return barrelCylinder->getUp();
}

protected:
  void localSimulate(double dt) override {
    //rotate( GMlib::Angle(30) * dt, GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ) );
  }

private:
  double arm_speed = 0.1f;
  bool gp_test {false};
  std::shared_ptr<ClosedCylinder> verticalPivot{nullptr};
  std::shared_ptr<ClosedCylinder> barrelCylinder{nullptr};
  //std::shared_ptr<ClosedCylinder> firstJoint{nullptr};
public slots:
  void rotateLeft(){
    verticalPivot->rotate( GMlib::Angle(2), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ) );
    emit newGunDirection(barrelCylinder->getUp());
  }
  void rotateRight(){
    verticalPivot->rotate( GMlib::Angle(-2), GMlib::Vector<float,3>( 0.0f, 0.0f, 1.0f ) );
  }
signals:
  void newGunDirection(GMlib::Vector<float,3> v);


}; // END class GunPlatform



#endif // GUNPLATFORM_H
